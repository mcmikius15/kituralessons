#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgTimeFunction.h>


@class SCDSvgTimeFunction;


/*PROTECTED REGION ID(3141371d6b89b500db677c0daf8377a1) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgSplineTimeFunction : SCDSvgTimeFunction


@property(nonatomic) NSArray<SCDSvgTimeFunction*>* _Nonnull keySplines;


/*PROTECTED REGION ID(5674dd822c7fd7c7da06bae6391f68e8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
