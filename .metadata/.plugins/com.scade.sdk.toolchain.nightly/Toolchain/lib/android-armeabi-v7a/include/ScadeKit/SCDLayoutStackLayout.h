#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutLayout.h>


@class SCDLayoutLayout;


/*PROTECTED REGION ID(9e96afb50f2f6dae297656dc39f3314d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutStackLayout : SCDLayoutLayout


/*PROTECTED REGION ID(a666d641fcb4a4c78b74165532931bc0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
