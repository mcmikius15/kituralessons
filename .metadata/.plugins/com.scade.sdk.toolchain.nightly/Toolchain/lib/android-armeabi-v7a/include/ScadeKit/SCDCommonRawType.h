#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(780a4ac280425d724cf1b6d83e081ecc) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCommonRawType : EObject


@property(nonatomic) id _Nullable content;


/*PROTECTED REGION ID(405933f79e52903cb0c6cf47476d8988) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
