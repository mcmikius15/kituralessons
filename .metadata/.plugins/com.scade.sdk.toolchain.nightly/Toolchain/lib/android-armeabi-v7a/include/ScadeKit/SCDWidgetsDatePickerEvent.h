#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(2f852d7be486298cbe00b15780d48d7c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsDatePickerEvent : SCDWidgetsEvent


@property(nonatomic) NSString* _Nonnull message;


/*PROTECTED REGION ID(0e07956cdc695afb5c64aeaba00faa60) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
