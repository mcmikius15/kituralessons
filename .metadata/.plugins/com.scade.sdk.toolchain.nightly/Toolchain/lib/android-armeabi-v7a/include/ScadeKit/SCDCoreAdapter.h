#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDCoreNotification;


/*PROTECTED REGION ID(5359b50d3e2c56a4088c315d13e01127) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCoreAdapter : EObject


- (void)notify:(SCDCoreNotification* _Nullable)notification;


/*PROTECTED REGION ID(b3d8c0583bda54e65fc9e2b601f8cbb3) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
