#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgConstraint.h>


@class SCDExprExpression;
@class SCDSvgConstraint;


/*PROTECTED REGION ID(3af2a05be7d93b8ca9c723db4849b7ce) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgScrollSizeConstraint : SCDSvgConstraint


@property(nonatomic) SCDExprExpression* _Nullable width;

@property(nonatomic) SCDExprExpression* _Nullable height;


/*PROTECTED REGION ID(b61bab6e9013d492cacf2133246bde67) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
