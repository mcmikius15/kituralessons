#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(13f2ae44be5912f42af82b792b246985) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathMove : SCDSvgPathElement


@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(ac1873a25ee8b153177e127fa97de84c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
