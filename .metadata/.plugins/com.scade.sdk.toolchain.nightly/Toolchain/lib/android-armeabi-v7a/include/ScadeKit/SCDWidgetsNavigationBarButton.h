#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsButton.h>


@class SCDWidgetsButton;

typedef NS_ENUM(NSInteger, SCDWidgetsNavigationBarButtonType);


/*PROTECTED REGION ID(f15619bf11c597b45630cd61adf396a0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsNavigationBarButton : SCDWidgetsButton


@property(nonatomic) SCDWidgetsNavigationBarButtonType type;


/*PROTECTED REGION ID(ff1dc31a781d9e433526c564f26e3938) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
