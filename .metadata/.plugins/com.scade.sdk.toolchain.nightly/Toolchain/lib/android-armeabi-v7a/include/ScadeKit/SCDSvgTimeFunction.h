#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(1c964bed266726e9a5d09c71d7562721) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgTimeFunction : EObject


- (float)evaluate:(float)time;


/*PROTECTED REGION ID(6440ec3111dfa8b87a958d3f035eb2c6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
