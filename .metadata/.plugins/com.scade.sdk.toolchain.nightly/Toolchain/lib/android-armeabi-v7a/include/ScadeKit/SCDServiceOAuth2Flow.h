#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceOAuthFlow.h>


@class SCDServiceOAuthFlow;


/*PROTECTED REGION ID(65a9d6a64525585b4225edd052354064) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceOAuth2Flow : SCDServiceOAuthFlow


/*PROTECTED REGION ID(25cb871d000f48e4839bed6b90bef5b4) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
