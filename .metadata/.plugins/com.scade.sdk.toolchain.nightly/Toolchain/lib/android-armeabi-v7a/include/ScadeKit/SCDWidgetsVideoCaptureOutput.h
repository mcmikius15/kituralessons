#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(71f76396303d1d99318ec06ebc347c14) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsVideoCaptureOutput : EObject


@property(nonatomic) NSData* _Nonnull data;

@property(nonatomic) long width;

@property(nonatomic) long height;

@property(nonatomic) long bytesPerRow;


/*PROTECTED REGION ID(c891df93b18db57d385a28c6e2ce9ed3) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
