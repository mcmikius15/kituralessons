#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDPlatformLocationCoordinate;


/*PROTECTED REGION ID(422dcce4581f3bbe748a116ca73d30af) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsMapRegion : EObject


@property(nonatomic) SCDPlatformLocationCoordinate* _Nonnull center;

@property(nonatomic) double latitudinalMeters;

@property(nonatomic) double longitudinalMeters;


/*PROTECTED REGION ID(9b5036e127adcbea473d105d6fa50936) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
