#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(b2967378d5a5c23a99f6bc9532ccb7f6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCommonHeader : EObject


@property(nonatomic) NSString* _Nonnull name;

@property(nonatomic) NSString* _Nonnull value;


/*PROTECTED REGION ID(83c8e91ee02caf5189752455a65576bf) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
