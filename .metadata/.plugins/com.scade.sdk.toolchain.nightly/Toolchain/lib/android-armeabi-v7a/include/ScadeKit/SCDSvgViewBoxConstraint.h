#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgConstraint.h>


@class SCDExprExpression;
@class SCDSvgConstraint;


/*PROTECTED REGION ID(bc6c028c8a74a75049754bb6483149ee) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgViewBoxConstraint : SCDSvgConstraint


@property(nonatomic) SCDExprExpression* _Nullable x;

@property(nonatomic) SCDExprExpression* _Nullable y;

@property(nonatomic) SCDExprExpression* _Nullable width;

@property(nonatomic) SCDExprExpression* _Nullable height;


/*PROTECTED REGION ID(3e9554a0422aa13130326b1a692384ba) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
