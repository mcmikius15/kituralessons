#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutGridLayout.h>


@class SCDLayoutGridLayout;


/*PROTECTED REGION ID(b270d564aa07c2a27fa87fd28c7c097a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsListLayout : SCDLayoutGridLayout


/*PROTECTED REGION ID(f84cd96043f2df1aeadee85aea470282) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
