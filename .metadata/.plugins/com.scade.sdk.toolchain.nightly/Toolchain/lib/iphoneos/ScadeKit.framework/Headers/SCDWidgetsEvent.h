#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class EObject;


/*PROTECTED REGION ID(d3bed240232195ceb42ae7228b949f7a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsEvent : EObject


@property(nonatomic) EObject* _Nullable target;


/*PROTECTED REGION ID(0b239c5129391564623c9afc1cb4a2d8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
