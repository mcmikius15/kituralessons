#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgBaseAnimation.h>


@class SCDSvgBaseAnimation;


/*PROTECTED REGION ID(c6578758b7149c5d7a7f0ea8fdbd0f69) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgTranslateAnimation : SCDSvgBaseAnimation


@property(nonatomic) float dx;

@property(nonatomic) float dy;


/*PROTECTED REGION ID(a7e1f1233cb55862fafc5b7246268ed7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
