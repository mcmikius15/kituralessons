#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsNavigationEvent.h>


@class SCDWidgetsNavigationEvent;


/*PROTECTED REGION ID(bc1b69b1a387ca9e1bb0f16329dc1177) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsEnterEvent : SCDWidgetsNavigationEvent


/*PROTECTED REGION ID(93c7f5b73b1f5e6625c9ee3eacfe973d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
