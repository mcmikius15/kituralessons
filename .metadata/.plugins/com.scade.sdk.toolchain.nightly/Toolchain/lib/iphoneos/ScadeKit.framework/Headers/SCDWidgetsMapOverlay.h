#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@protocol SCDSvgDrawable;


/*PROTECTED REGION ID(8e0941f4f055dbf6c5a12c65c6589086) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsMapOverlay : EObject


@property(nonatomic) id<SCDSvgDrawable> _Nullable drawing;


/*PROTECTED REGION ID(d62dbe1532f127343f496cbfdc7b4ec0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
