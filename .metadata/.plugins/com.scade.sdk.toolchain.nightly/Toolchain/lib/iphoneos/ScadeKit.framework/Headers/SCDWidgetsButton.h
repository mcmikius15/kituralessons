#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsTextWidget.h>
#import <ScadeKit/SCDWidgetsClickable.h>


@protocol SCDWidgetsClickable;

@class SCDWidgetsTextWidget;


/*PROTECTED REGION ID(1a091bdd5c2c84ae6f1c8c2ee5e4056a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsButton : SCDWidgetsTextWidget <SCDWidgetsClickable>


/*PROTECTED REGION ID(17b6ebb7786de3618accff69b835cea0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
