#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>


@class SCDWidgetsDatePickerEventHandler;
@class SCDWidgetsWidget;


/*PROTECTED REGION ID(18618e22c9b76478c40123c36cafe883) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsDatePicker : SCDWidgetsWidget


@property(nonatomic)
    NSArray<SCDWidgetsDatePickerEventHandler*>* _Nonnull onDatePick;


/*PROTECTED REGION ID(8f788965e8fe409355c480b565a991c8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
