#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsTextWidget.h>


@class SCDWidgetsTextWidget;


/*PROTECTED REGION ID(15732e6f93da147180870ae85c693ee5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsLabel : SCDWidgetsTextWidget


/*PROTECTED REGION ID(0ab751fadbe40f97bab083695d653bff) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
