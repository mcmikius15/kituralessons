#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDSvgGestureRecognizer;


/*PROTECTED REGION ID(5e5c952914ab17d11350fbdbebe0f229) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgTouchReceiver <EObject>


@property(nonatomic)
    NSArray<SCDSvgGestureRecognizer*>* _Nonnull gestureRecognizers;


/*PROTECTED REGION ID(dbe9ca6f6b9360d65e5c6c2f9748eaf1) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
