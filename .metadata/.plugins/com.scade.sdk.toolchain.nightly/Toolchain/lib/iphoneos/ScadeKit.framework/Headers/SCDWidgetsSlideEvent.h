#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(1146688edc2a0faf48ffa0756789ab7e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsSlideEvent : SCDWidgetsEvent


@property(nonatomic) long from;

@property(nonatomic) long to;


/*PROTECTED REGION ID(65ec9c1d54e016f47d0f8ca803f97111) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
