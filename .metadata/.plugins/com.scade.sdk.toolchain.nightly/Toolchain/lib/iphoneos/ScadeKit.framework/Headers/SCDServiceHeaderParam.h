#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceParam.h>


@class SCDServiceParam;


/*PROTECTED REGION ID(cfe7dac87391b28709ca977b2d94013e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceHeaderParam : SCDServiceParam


/*PROTECTED REGION ID(1f9459ccd716283117ff8aa1cd3617fe) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
