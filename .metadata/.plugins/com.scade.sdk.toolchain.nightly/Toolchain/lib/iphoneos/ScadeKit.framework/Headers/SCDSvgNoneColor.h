#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgColor.h>


@class SCDSvgColor;


/*PROTECTED REGION ID(69ee40c64ba290392fcdc9c7db82648b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgNoneColor : SCDSvgColor


/*PROTECTED REGION ID(d9902d728f8ea175ebdbcc13956dacc5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
