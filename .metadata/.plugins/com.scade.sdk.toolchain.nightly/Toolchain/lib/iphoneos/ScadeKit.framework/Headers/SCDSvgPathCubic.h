#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(d190ed62b8ec9566af1cdf306ffb64f7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathCubic : SCDSvgPathElement


@property(nonatomic) float x1;

@property(nonatomic) float y1;

@property(nonatomic) float x2;

@property(nonatomic) float y2;

@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(a0a984433257f6be7b42cbbbe7a3b463) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
