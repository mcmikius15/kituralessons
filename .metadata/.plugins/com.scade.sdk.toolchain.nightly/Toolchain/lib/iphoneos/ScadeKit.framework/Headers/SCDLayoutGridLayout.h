#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutLayout.h>


@class SCDLayoutLayout;


/*PROTECTED REGION ID(550f66ec9d5ba4c95a29606382bf2bed) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutGridLayout : SCDLayoutLayout


@property(nonatomic) long columns;

@property(nonatomic) long rows;

@property(nonatomic) long verticalSpacing;

@property(nonatomic) long horizontalSpacing;

@property(nonatomic) long marginTop;

@property(nonatomic) long marginBottom;

@property(nonatomic) long marginLeft;

@property(nonatomic) long marginRight;


/*PROTECTED REGION ID(dc974e9f88d127378b1cd37e938170c2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
