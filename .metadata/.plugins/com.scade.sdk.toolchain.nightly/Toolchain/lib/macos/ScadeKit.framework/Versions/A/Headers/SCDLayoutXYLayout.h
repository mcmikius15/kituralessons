#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutLayout.h>


@class SCDLayoutLayout;


/*PROTECTED REGION ID(1abf537aadae48bb7848f0b9963e8e66) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutXYLayout : SCDLayoutLayout


/*PROTECTED REGION ID(a47dafad643645ff7a68a3cbc03817be) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
