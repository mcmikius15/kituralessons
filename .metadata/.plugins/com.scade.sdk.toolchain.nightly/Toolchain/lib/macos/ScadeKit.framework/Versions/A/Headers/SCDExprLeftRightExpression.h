#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprExpression.h>


@class SCDExprExpression;


/*PROTECTED REGION ID(5cf26a395b66498f0b2f9029b959223d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprLeftRightExpression : SCDExprExpression


@property(nonatomic) SCDExprExpression* _Nullable left;

@property(nonatomic) SCDExprExpression* _Nullable right;


/*PROTECTED REGION ID(f3452a47e2f187018c8db06fd753b5f6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
