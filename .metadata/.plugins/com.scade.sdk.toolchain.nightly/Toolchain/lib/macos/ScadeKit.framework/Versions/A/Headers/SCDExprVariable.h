#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprExpression.h>


@class SCDExprExpression;


/*PROTECTED REGION ID(ff070bcefc9ba3c633a595aadc98a33f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprVariable : SCDExprExpression


@property(nonatomic) NSString* _Nonnull name;

@property(nonatomic) NSArray<NSString*>* _Nonnull path;


/*PROTECTED REGION ID(1f1f954a93eba56a4c10ffd22adf127d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
