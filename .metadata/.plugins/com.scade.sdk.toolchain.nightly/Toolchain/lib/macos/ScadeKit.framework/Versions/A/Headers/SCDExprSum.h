#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprLeftRightExpression.h>


@class SCDExprLeftRightExpression;


/*PROTECTED REGION ID(51d9834b18cf7a03f8d81bc3df61588a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprSum : SCDExprLeftRightExpression


/*PROTECTED REGION ID(3da00916eb76dfc251994f8228ae5f31) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
