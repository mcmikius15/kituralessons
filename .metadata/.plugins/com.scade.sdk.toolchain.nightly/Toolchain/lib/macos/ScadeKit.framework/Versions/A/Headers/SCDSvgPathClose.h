#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(00404dce3c49b6d1d16d3156edd8e7a8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathClose : SCDSvgPathElement


@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(80514851d6b6ef9863ded581547e54e0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
