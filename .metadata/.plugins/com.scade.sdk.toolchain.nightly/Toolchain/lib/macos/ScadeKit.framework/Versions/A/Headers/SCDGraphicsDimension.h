#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(bcbef0ab10b98bca17f83f231637398c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDGraphicsDimension : EObject


@property(nonatomic) long width;

@property(nonatomic) long height;


/*PROTECTED REGION ID(9b1777585a44cc32a4f796a7fc00f9ce) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
