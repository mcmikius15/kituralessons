#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDWidgetsEventHandler;


/*PROTECTED REGION ID(a30aaa6d4830ece20f3c287e8427d82c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDWidgetsClickable <EObject>


@property(nonatomic) NSArray<SCDWidgetsEventHandler*>* _Nonnull onClick;


/*PROTECTED REGION ID(23a84572995b895de6245e0c1c551144) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
