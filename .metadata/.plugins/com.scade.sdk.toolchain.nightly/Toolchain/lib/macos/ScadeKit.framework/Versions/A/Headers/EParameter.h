#import <Foundation/Foundation.h>

#import <ScadeKit/ETypedElement.h>


@class EOperation;
@class ETypedElement;


/*PROTECTED REGION ID(b92ccc0c5bb285b5e3b3ea202cc7b529) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface EParameter : ETypedElement


@property(nonatomic, readonly) EOperation* _Nullable eOperation;


/*PROTECTED REGION ID(ed23f92965d931c6e283d095bf02d9eb) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
