#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>


@class SCDWidgetsContainer;


/*PROTECTED REGION ID(637ab349e894a490564da00872fa0cc9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsListView : SCDWidgetsContainer


/*PROTECTED REGION ID(f01765db357fdedc3f33f1804ce35075) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
