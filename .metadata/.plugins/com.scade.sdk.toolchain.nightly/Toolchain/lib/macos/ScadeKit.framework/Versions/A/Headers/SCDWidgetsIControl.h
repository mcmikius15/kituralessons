#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(b62b9fd9688aed9173e098990e83261d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDWidgetsIControl <EObject>


@property(nonatomic) long tag;

@property(nonatomic) NSString* _Nonnull name;


/*PROTECTED REGION ID(945f58cef9cdaefbbcf06aec3d9a2767) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
