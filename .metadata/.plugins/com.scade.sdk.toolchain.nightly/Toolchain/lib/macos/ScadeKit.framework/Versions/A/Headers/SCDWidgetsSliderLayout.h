#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutGridLayout.h>


@class SCDLayoutGridLayout;


/*PROTECTED REGION ID(b4d0e1ed5e215fe9a474edd9e933f132) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsSliderLayout : SCDLayoutGridLayout


/*PROTECTED REGION ID(0f84c74ad3bbb39f6367a55de8abfafa) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
