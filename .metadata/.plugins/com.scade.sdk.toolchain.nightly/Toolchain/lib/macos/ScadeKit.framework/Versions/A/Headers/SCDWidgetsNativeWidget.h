#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>


@class SCDWidgetsWidget;


/*PROTECTED REGION ID(93a19e47b8451fe31152b94c0f0de75e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsNativeWidget : SCDWidgetsWidget


/*PROTECTED REGION ID(7e7a1890dd3deec52c7d23fdd1cfa547) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
