#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDPlatformContact;


/*PROTECTED REGION ID(dc945d9a076574599ed559b8ec8d9554) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformContactSearchResult : EObject


@property(nonatomic) NSArray<SCDPlatformContact*>* _Nonnull contacts;


/*PROTECTED REGION ID(3d11dceec3170093b164db8a46136c8d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
