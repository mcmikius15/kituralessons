#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLatticeView.h>


@protocol SCDLatticeView;

@class SCDWidgetsPage;


/*PROTECTED REGION ID(99ef5546b9de50f4980762971317a93f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLatticeWindow : EObject <SCDLatticeView>


/*PROTECTED REGION ID(30030e7e608718e656fdb111b2f3e9b8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
