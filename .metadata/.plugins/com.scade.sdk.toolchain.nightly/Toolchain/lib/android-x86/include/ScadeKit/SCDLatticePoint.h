#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(e048c2bb71ac34476d360bb4601df73b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLatticePoint : EObject


@property(nonatomic) NSString* _Nonnull name;


/*PROTECTED REGION ID(93a2d28f1cfcaab25c172fbdd9dbc1d5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
