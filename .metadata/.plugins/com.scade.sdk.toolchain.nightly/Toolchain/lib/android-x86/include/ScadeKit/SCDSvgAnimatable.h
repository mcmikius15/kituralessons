#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDSvgAnimation;


/*PROTECTED REGION ID(94b44a517cd6b057a82ef07d27955c66) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgAnimatable <EObject>


@property(nonatomic) NSArray<SCDSvgAnimation*>* _Nonnull animations;


/*PROTECTED REGION ID(742d6f03df56f11b83f51fda61e8af8b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
