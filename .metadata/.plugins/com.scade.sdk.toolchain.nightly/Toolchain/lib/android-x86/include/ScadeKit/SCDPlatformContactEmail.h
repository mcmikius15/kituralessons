#import <Foundation/Foundation.h>

#import <ScadeKit/SCDPlatformContactLabel.h>


@class SCDPlatformContactLabel;


/*PROTECTED REGION ID(e1527fadf456c1be33dc54623e38c91d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformContactEmail : SCDPlatformContactLabel


@property(nonatomic) NSString* _Nonnull email;


/*PROTECTED REGION ID(ae960e879fe45d63cdf49c1ff2411301) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
