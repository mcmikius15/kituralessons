#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(7a5059ce0af42be5b214403a45a47ab2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCommonFileType : EObject


@property(nonatomic) NSString* _Nonnull path;


/*PROTECTED REGION ID(1e4ab088681e43392cdff83111d251c9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
