#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(61ab5995fc217ec3edec4cee0557a50c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceOAuthCredential : EObject


@property(nonatomic) NSString* _Nonnull key;

@property(nonatomic) NSString* _Nonnull secret;


/*PROTECTED REGION ID(8efe361f43928955f0a296e29d964d73) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
