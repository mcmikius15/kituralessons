#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprUnaryExpression.h>


@class SCDExprUnaryExpression;


/*PROTECTED REGION ID(ac5a54cfad303bfe508394eb3d78129f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprNegativeExpression : SCDExprUnaryExpression


/*PROTECTED REGION ID(9bf42932a727bdde3ec017ce626e74e1) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
