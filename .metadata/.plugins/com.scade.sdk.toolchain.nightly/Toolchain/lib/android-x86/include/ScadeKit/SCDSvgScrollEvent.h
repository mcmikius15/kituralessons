#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDSvgScrollGroup;
@class SCDGraphicsPointF;


/*PROTECTED REGION ID(d5e777ebb8bfa03fcc516dff4bc0932e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgScrollEvent : EObject


@property(nonatomic) SCDSvgScrollGroup* _Nullable target;

@property(nonatomic) SCDGraphicsPointF* _Nonnull location;


/*PROTECTED REGION ID(26046a2fca7cafa77985e4cb19c89e12) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
