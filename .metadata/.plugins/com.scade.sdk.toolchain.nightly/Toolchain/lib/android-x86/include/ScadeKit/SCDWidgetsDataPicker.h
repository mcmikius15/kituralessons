#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>


@class SCDWidgetsDataWheel;
@class SCDWidgetsWidget;


/*PROTECTED REGION ID(c27a37dc8821ead698f2ef7b11479689) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsDataPicker : SCDWidgetsWidget


@property(nonatomic) NSArray<SCDWidgetsDataWheel*>* _Nonnull wheels;

@property(nonatomic) long spacing;


/*PROTECTED REGION ID(a7126906a8f6097b20ecefb9522378a1) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
