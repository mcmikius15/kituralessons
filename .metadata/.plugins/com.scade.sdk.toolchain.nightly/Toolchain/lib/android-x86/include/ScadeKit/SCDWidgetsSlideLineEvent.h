#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(c304eeccb6f6707658e29f9b401ade4b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsSlideLineEvent : SCDWidgetsEvent


@property(nonatomic) long oldValue;

@property(nonatomic) long newValue;


/*PROTECTED REGION ID(e5ae642fe77f01e63319f5b5ddcc0cc2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
