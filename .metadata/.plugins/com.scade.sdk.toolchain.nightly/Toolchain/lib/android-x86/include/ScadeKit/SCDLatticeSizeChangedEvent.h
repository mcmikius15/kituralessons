#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDGraphicsDimension;


/*PROTECTED REGION ID(84b48a3e2bb994703feb3ff67d9980f9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLatticeSizeChangedEvent : EObject


@property(nonatomic) SCDGraphicsDimension* _Nonnull oldSize;

@property(nonatomic) SCDGraphicsDimension* _Nonnull newSize;


/*PROTECTED REGION ID(117c5da8b9b61750a199bfbacb3d5c2b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
