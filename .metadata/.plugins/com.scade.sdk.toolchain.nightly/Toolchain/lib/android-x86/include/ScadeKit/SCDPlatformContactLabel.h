#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


typedef NS_ENUM(NSInteger, SCDPlatformContactLabelKey);


/*PROTECTED REGION ID(c1fe13af2a5913b0c6333db55bf24696) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformContactLabel : EObject


@property(nonatomic) SCDPlatformContactLabelKey key;

@property(nonatomic) NSString* _Nonnull customKey;


/*PROTECTED REGION ID(dc9bc702f5dc5240fb4c97df957c7993) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
