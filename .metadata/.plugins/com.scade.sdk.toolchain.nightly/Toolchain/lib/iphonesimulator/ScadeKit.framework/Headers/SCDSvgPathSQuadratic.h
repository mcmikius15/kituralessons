#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(d69ef38f096ef32bc12cbb646cc59770) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathSQuadratic : SCDSvgPathElement


@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(3fd4a6121ba5aa3bc5d2c03e88ea5d97) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
