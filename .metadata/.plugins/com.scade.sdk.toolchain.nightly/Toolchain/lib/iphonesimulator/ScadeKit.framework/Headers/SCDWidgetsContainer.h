#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>
#import <ScadeKit/SCDWidgetsIContainer.h>


@protocol SCDWidgetsIContainer;

@class SCDWidgetsWidget;


/*PROTECTED REGION ID(27aa89c284e3a71dac81d0df53bcc815) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsContainer : SCDWidgetsWidget <SCDWidgetsIContainer>


/*PROTECTED REGION ID(a5cd262f84802beac6d5e23e01ba66ef) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
