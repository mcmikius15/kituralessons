#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgDrawable.h>
#import <ScadeKit/SCDSvgStylable.h>


@protocol SCDSvgDrawable;
@protocol SCDSvgStylable;


/*PROTECTED REGION ID(fbc2d1dd6e4cd70e80713485963e4d49) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgShape <SCDSvgDrawable, SCDSvgStylable>


/*PROTECTED REGION ID(a53ce8668ae853b2b8aa95b6702ed894) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
