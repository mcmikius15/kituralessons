#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgContainerElement.h>
#import <ScadeKit/SCDSvgDrawable.h>


@protocol SCDSvgDrawable;

@class SCDSvgContainerElement;


/*PROTECTED REGION ID(4339ec68a6a51f2b63958276a06759da) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgClipPath : SCDSvgContainerElement <SCDSvgDrawable>


/*PROTECTED REGION ID(56d0a08f893448ceed5832e428486c9d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
