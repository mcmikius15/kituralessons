#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsNavigationEvent.h>


@class SCDWidgetsNavigationEvent;


/*PROTECTED REGION ID(dd37352cb0d1845eff168c78ac4cd204) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsExitEvent : SCDWidgetsNavigationEvent


/*PROTECTED REGION ID(5cb35502ca28e0532fa97184fb51c752) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
