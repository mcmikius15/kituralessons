#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprUnaryExpression.h>


@class SCDExprUnaryExpression;


/*PROTECTED REGION ID(ac3afa4b2adccfe1e771e54c1053d2c6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprPositiveExpression : SCDExprUnaryExpression


/*PROTECTED REGION ID(c77f4fc896656b7fdbbe75f35bcec483) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
