#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(bd8b7d1d5b81f903d952bcb3846166c7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathSCubic : SCDSvgPathElement


@property(nonatomic) float x2;

@property(nonatomic) float y2;

@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(0c365b514b97172d2d0af3c1e12a6988) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
