#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(da0d192fe4907c04c93f4dd06be5d587) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCommonJsonType : EObject


@property(nonatomic) NSString* _Nonnull content;


/*PROTECTED REGION ID(ebfe27760bd257e21570d8baa5905022) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
