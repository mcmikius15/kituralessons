#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgAnimation.h>


@class SCDSvgAnimation;


/*PROTECTED REGION ID(e34eaf51d55fd7ad130fd50a6aa823e6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgGroupAnimation : SCDSvgAnimation


@property(nonatomic) NSArray<SCDSvgAnimation*>* _Nonnull animations;


/*PROTECTED REGION ID(d0f3e3a0124a9e8164562c7d32820785) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
