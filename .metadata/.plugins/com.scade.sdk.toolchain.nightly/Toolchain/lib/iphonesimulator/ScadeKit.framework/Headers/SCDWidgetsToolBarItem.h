#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>
#import <ScadeKit/SCDWidgetsClickable.h>


@protocol SCDWidgetsClickable;

@class SCDWidgetsContainer;


/*PROTECTED REGION ID(7e5cd77805282634231eb12d0781fccc) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsToolBarItem : SCDWidgetsContainer <SCDWidgetsClickable>


/*PROTECTED REGION ID(fec475483f7a20a897f90d322be07e55) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
