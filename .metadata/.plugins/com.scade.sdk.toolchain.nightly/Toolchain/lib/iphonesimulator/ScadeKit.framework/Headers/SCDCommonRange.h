#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(79442c97251de04249e51884ef76aa33) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCommonRange : EObject


@property(nonatomic) long start;

@property(nonatomic) long end;


/*PROTECTED REGION ID(d220f053e0c5d4bc7fe33de6ed016bea) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
