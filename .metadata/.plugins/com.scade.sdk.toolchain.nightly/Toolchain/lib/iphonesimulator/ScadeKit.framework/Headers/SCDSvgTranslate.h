#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgTransform.h>


@protocol SCDSvgTransform;


/*PROTECTED REGION ID(db55441a3e45c0e972fd100009ec11a8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgTranslate : EObject <SCDSvgTransform>


@property(nonatomic) float translateX;

@property(nonatomic) float translateY;


/*PROTECTED REGION ID(3770d01d54a08f756a08f7feeb1f4e4a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
