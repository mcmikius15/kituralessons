#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprExpression.h>


@class SCDExprExpression;


/*PROTECTED REGION ID(655c5b36193cc841eba4e05fb7f7a199) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprNumber : SCDExprExpression


@property(nonatomic) float value;


/*PROTECTED REGION ID(07098d4b30f7a279ddce72265f60af58) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
