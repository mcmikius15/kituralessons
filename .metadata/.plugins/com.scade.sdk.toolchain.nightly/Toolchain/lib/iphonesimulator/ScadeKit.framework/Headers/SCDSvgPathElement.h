#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(65687f32d37c1cf8b16eb3db98e0b39c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathElement : EObject


@property(nonatomic, getter=isAbsolute) BOOL absolute;


/*PROTECTED REGION ID(5e79a3ce8e45d0cc736a3b4a98e9feaf) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
