#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgTimeFunction.h>


@class SCDSvgTimeFunction;


/*PROTECTED REGION ID(e47153b493fe8c1b9da3c98cb2accfa4) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgCubicTimeFunction : SCDSvgTimeFunction


@property(nonatomic) float x1;

@property(nonatomic) float y1;

@property(nonatomic) float x2;

@property(nonatomic) float y2;


/*PROTECTED REGION ID(69e55c647ecd3788abeaf94eac06e660) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
