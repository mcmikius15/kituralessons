#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprLeftRightExpression.h>


@class SCDExprLeftRightExpression;


/*PROTECTED REGION ID(ba72cc5836c861e7006e15d752748ac7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprDivision : SCDExprLeftRightExpression


/*PROTECTED REGION ID(14dd4cc796a46aee403388a68e2b4d82) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
