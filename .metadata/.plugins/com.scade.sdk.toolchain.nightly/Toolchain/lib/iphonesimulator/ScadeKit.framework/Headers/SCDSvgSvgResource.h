#import <Foundation/Foundation.h>

#import <ScadeKit/SCDCoreResource.h>


@class SCDCoreResource;
@class EObject;


/*PROTECTED REGION ID(5df8889e535921ebc34b2b84b5f86380) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgSvgResource : SCDCoreResource


/*PROTECTED REGION ID(56f6ff405fcef8620234f312d2b2ea6f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
