#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>
#import <ScadeKit/SCDLatticeView.h>


@protocol SCDLatticeView;

@class SCDWidgetsWidget;
@class SCDWidgetsPage;


/*PROTECTED REGION ID(a9aa64b0f548366e4f3d74912d1bfe0d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLatticePageContainer : SCDWidgetsWidget <SCDLatticeView>


/*PROTECTED REGION ID(4adff2d79785a42fde0f9bb805e09e80) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
