#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


typedef NS_ENUM(NSInteger, SCDPlatformContactIMLabelKey);


/*PROTECTED REGION ID(3680f6d3676b09e82155babff264d5e2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformContactIM : EObject


@property(nonatomic) NSString* _Nonnull url;

@property(nonatomic) SCDPlatformContactIMLabelKey key;

@property(nonatomic) NSString* _Nonnull customKey;


/*PROTECTED REGION ID(6cd3efbb8f744f5557ba6955f3ed9376) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
