#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(da2dffa0d4e90052e618fad4700af618) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceHttpCredential : EObject


@property(nonatomic) NSString* _Nonnull username;

@property(nonatomic) NSString* _Nonnull password;


/*PROTECTED REGION ID(32f8797bc6d4ae641ad0412a3bf6f709) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
