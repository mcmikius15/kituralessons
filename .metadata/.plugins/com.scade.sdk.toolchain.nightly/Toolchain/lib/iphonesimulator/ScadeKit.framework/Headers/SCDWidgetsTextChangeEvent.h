#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(e5641654abf2bb774dde2fe382463080) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsTextChangeEvent : SCDWidgetsEvent


@property(nonatomic) NSString* _Nonnull oldValue;

@property(nonatomic) NSString* _Nonnull newValue;


/*PROTECTED REGION ID(8c4c1c489069fec3abba566f96cfda5c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
