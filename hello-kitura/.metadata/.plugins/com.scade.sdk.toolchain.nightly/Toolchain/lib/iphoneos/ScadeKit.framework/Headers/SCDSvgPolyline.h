#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgShape.h>


@protocol SCDSvgShape;


/*PROTECTED REGION ID(47bcffbccd1ce5caa70914a86e411395) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPolyline : EObject <SCDSvgShape>


@property(nonatomic, getter=isClosed) BOOL closed;


/*PROTECTED REGION ID(2091a4e361a5f0171737e1f113fc851b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
