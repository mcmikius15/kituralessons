#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(66ab9ae422ee67b83cc6487383a4250f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDNetworkContentPart : EObject


@property(nonatomic) NSString* _Nonnull content;

@property(nonatomic) NSString* _Nonnull type;


/*PROTECTED REGION ID(a0e1af70cda8d2a1f4c7757f6a227cc2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
