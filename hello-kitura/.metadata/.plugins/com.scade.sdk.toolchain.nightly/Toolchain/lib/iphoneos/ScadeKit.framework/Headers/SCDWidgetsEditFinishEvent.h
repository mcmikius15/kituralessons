#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(0d379d3617be359a9472464e283874c6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsEditFinishEvent : SCDWidgetsEvent


@property(nonatomic) NSString* _Nonnull oldValue;

@property(nonatomic) NSString* _Nonnull newValue;


/*PROTECTED REGION ID(00a0dac6d2bda640641147dc42544423) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
