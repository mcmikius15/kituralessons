#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(69d98c39052585caecb5b4b784a4f580) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgAccessibility <EObject>


@property(nonatomic) NSString* _Nonnull accessibilityLabel;


/*PROTECTED REGION ID(883b3fc56d1bae61bb1e8cf15c94b310) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
