#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(aa7e5f00c780b2e7b8c4187fb78feba6) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgTextSegment <EObject>


@property(nonatomic) NSString* _Nonnull text;


/*PROTECTED REGION ID(3ded6860db3eafd4fe8793a91d22b19a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
