#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceOAuthFlow.h>


@class SCDServiceOAuthFlow;


/*PROTECTED REGION ID(e8473f86b0e3c0a8b68fd4c476cbc798) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceOAuth1Flow : SCDServiceOAuthFlow


@property(nonatomic) NSString* _Nonnull requestTokenUrl;

@property(nonatomic) NSString* _Nonnull redirectedParameterForToken;


/*PROTECTED REGION ID(77490b4e5e7eefc5caa19b017f3f0c75) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
