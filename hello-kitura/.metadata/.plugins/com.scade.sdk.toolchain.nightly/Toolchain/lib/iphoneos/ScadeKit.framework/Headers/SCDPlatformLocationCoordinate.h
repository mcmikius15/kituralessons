#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(7b5ecc12edb7ddbfbec0f76ed57e0ff7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformLocationCoordinate : EObject


@property(nonatomic) double latitude;

@property(nonatomic) double longitude;


/*PROTECTED REGION ID(d7dbb42423c437ce8e71ab397b21f57c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
