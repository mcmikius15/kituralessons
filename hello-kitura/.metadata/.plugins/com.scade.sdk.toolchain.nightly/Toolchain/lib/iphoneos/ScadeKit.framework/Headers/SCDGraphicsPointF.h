#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(9894207377da22033def7ed54e6306cc) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDGraphicsPointF : EObject


@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(6f482b705cfc1c8febff8324c6afd112) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
