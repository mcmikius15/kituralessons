#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


typedef NS_ENUM(NSInteger, SCDPlatformCameraSourceType);


/*PROTECTED REGION ID(7e17eae0f75ce5897b4ede591af7e962) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformCameraOptions : EObject


@property(nonatomic) SCDPlatformCameraSourceType sourceType;


/*PROTECTED REGION ID(4d54411e983051a3aaf618f9eb97fcf4) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
