#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(1a931dde3df9a5a2b2ef7055e81a3101) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceOAuthFlow : EObject


@property(nonatomic) NSString* _Nonnull providerName;

@property(nonatomic) NSString* _Nonnull authorizeUrl;

@property(nonatomic) NSString* _Nonnull accessTokenUrl;


/*PROTECTED REGION ID(4954e76d4e39a37250b4c36eb54883c7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
