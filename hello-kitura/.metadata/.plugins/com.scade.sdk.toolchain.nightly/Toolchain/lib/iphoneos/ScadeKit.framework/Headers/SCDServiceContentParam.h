#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceParam.h>


@class SCDServiceParam;


/*PROTECTED REGION ID(19c4fdd029ca51f6cae6ce6684392e01) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceContentParam : SCDServiceParam


@property(nonatomic) long index;


/*PROTECTED REGION ID(547f00b681f4f142b9856308489d805b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
