#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsEvent;


/*PROTECTED REGION ID(b0a92a9d8afd292bf6ad8eba61e689f5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsNavigationEvent : SCDWidgetsEvent


@property(nonatomic) id _Nullable data;


/*PROTECTED REGION ID(101ba8ac208e103b6470c2fb7dd97244) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
