#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutILayoutable.h>
#import <ScadeKit/SCDWidgetsIVisualControl.h>


@protocol SCDLayoutILayoutable;
@protocol SCDWidgetsIVisualControl;


/*PROTECTED REGION ID(ec2345ee172e336ceedcf014560fe0f2) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDWidgetsLayoutable <SCDLayoutILayoutable, SCDWidgetsIVisualControl>


/*PROTECTED REGION ID(3175861406d361c794968e9d72ce5480) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
