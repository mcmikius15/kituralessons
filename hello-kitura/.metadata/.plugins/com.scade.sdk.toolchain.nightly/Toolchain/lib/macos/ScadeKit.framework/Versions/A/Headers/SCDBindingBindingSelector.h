#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(0a4c8837c42d15256aa44e084a0f9c0d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDBindingBindingSelector : EObject


@property(nonatomic) NSString* _Nonnull feature;

@property(nonatomic) SCDBindingBindingSelector* _Nullable next;


/*PROTECTED REGION ID(615c61ed00b382e347656fff07913228) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
