#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprExpression.h>


@class SCDExprExpression;


/*PROTECTED REGION ID(163a9e1d66c085b50ab4a169c55ccaee) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprUnaryExpression : SCDExprExpression


@property(nonatomic) SCDExprExpression* _Nullable argument;


/*PROTECTED REGION ID(c353d6418f25802aa41e645a5546b919) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
