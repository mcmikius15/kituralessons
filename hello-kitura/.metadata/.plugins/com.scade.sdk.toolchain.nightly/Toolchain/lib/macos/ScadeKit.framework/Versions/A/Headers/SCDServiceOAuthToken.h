#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(e7c8efb275ff66a77792b80072ec88e5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceOAuthToken : EObject


@property(nonatomic) NSString* _Nonnull accessToken;

@property(nonatomic) NSString* _Nonnull refreshToken;


/*PROTECTED REGION ID(1c8ec0a2f9fd4dda8cd30b330405d1d9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
