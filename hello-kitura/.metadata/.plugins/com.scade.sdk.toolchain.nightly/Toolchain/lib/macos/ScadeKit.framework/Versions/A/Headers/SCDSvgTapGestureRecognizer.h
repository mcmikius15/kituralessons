#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgGestureRecognizer.h>


@class SCDSvgGestureRecognizer;
@class SCDSvgTouchEvent;

typedef NS_ENUM(NSInteger, SCDSvgTouchHandlerState);


/*PROTECTED REGION ID(5c09cdedb24912f43725d4490c6bb606) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgTapGestureRecognizer : SCDSvgGestureRecognizer


@property(nonatomic) float numTaps;


/*PROTECTED REGION ID(b416172a68d551db842d5a9507d5166f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
