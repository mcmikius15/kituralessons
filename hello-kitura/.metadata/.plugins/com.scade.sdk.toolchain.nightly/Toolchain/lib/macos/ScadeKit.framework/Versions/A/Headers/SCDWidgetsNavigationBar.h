#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>


@class SCDWidgetsContainer;


/*PROTECTED REGION ID(2710562559e9d874d20c279d75080154) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsNavigationBar : SCDWidgetsContainer


/*PROTECTED REGION ID(54be25c80c79f0a92d1d9fc8f014eb48) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
