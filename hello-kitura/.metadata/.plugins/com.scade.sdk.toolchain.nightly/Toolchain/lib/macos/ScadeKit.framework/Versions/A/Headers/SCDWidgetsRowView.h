#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>


@class SCDWidgetsContainer;


/*PROTECTED REGION ID(ee618b9117796839776a9b795f53355f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsRowView : SCDWidgetsContainer


/*PROTECTED REGION ID(1fde77bd2360ed867d537b70654b4591) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
