#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(6eb7aab581a1ce0234976b7829081ded) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutGridStyle : EObject


@property(nonatomic) long verticalSpacing;

@property(nonatomic) long horizontalSpacing;

@property(nonatomic) long marginTop;

@property(nonatomic) long marginBottom;

@property(nonatomic) long marginLeft;

@property(nonatomic) long marginRight;


/*PROTECTED REGION ID(8ac6659400fb147df3ce0e9ffdaa0967) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
