#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(86752a8335f8247115be859e3b9d7839) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathLine : SCDSvgPathElement


@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(798899a54bc75fc290c480471f5d4ffe) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
