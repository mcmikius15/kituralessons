#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(b7cfb312bf742e6ddc82220f986b6842) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathHLine : SCDSvgPathElement


@property(nonatomic) float x;


/*PROTECTED REGION ID(d4138d80e54f5b0c2fd67419a1a33d03) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
