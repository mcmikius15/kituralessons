#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDBindingBindingSelector;


/*PROTECTED REGION ID(f15d15598acb209dfa0f3346251f7222) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDBindingBindingSyncPoint : EObject


@property(nonatomic) SCDBindingBindingSelector* _Nullable srcSelector;

@property(nonatomic) SCDBindingBindingSelector* _Nullable dstSelector;

@property(nonatomic) SCDBindingBindingSyncPoint* _Nullable next;


/*PROTECTED REGION ID(5d4626da4546863c21f166096070966d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
