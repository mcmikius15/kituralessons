#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@protocol SCDLayoutILayoutable;


/*PROTECTED REGION ID(476850f5fe0975aab48d73edd80c3d04) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutLayout : EObject


@property(nonatomic, readonly) id<SCDLayoutILayoutable> _Nonnull layoutable;


/*PROTECTED REGION ID(68a2dff9be76a5cc683d1af4334a91b9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
