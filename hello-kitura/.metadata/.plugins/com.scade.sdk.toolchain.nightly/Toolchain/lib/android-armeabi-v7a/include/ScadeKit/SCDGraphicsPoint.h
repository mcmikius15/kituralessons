#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(3f87066c6bca0cacef274491faed5e92) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDGraphicsPoint : EObject


@property(nonatomic) long x;

@property(nonatomic) long y;


/*PROTECTED REGION ID(39236a75129bd7f7cf9f68bd80b78089) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
