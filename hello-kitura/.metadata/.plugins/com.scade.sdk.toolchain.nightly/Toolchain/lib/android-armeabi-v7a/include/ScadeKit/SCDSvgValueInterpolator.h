#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgValueFunction.h>


@protocol SCDSvgValueFunction;


/*PROTECTED REGION ID(8b1a90d99188e18694131cfcfb4b833a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgValueInterpolator : EObject <SCDSvgValueFunction>


@property(nonatomic) NSArray<id>* _Nonnull values;


/*PROTECTED REGION ID(b9f3fbc965297f56b0b2344e47e8f575) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
