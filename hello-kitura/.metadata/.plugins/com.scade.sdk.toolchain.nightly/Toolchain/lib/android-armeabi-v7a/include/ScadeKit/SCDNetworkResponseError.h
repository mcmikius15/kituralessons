#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(2c1e6b882c8b427ae7798e4cd9880014) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDNetworkResponseError : EObject


@property(nonatomic) NSString* _Nonnull message;

@property(nonatomic) long code;


/*PROTECTED REGION ID(157576da96a2e31d238e0ccad9508702) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
