#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>


@class SCDWidgetsContainer;


/*PROTECTED REGION ID(e6db4743c8a3da2e0c11b4f118dbeef5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsToolBar : SCDWidgetsContainer


/*PROTECTED REGION ID(9505b48850bdde308ce66811ad6f6bac) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
