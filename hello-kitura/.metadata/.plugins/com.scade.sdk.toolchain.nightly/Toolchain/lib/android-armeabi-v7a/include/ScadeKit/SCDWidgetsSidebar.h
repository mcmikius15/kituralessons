#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsContainer.h>


@class SCDWidgetsContainer;


/*PROTECTED REGION ID(7578995a30ac78a47bd53bebcf9d0a34) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsSidebar : SCDWidgetsContainer


@property(nonatomic) SCDWidgetsContainer* _Nullable panel;

@property(nonatomic, getter=isHidden) BOOL hidden;


/*PROTECTED REGION ID(292c6991e500ab25e7850c9a076461c5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
