#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgPathElement.h>


@class SCDSvgPathElement;


/*PROTECTED REGION ID(745073ceb226b1cd115181da3430e8de) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgPathQuadratic : SCDSvgPathElement


@property(nonatomic) float x1;

@property(nonatomic) float y1;

@property(nonatomic) float x;

@property(nonatomic) float y;


/*PROTECTED REGION ID(28573b5af54327f89198bf1d29369c56) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
