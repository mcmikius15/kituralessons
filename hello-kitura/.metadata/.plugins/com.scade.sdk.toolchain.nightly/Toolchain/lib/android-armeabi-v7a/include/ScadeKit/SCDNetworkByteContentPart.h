#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(f8e25c622ed3fd0d75cebcf8ac3db76a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDNetworkByteContentPart : EObject


@property(nonatomic) NSData* _Nonnull content;

@property(nonatomic) NSString* _Nonnull type;


/*PROTECTED REGION ID(708c70ee8a33e0d63c6559a6ab327d6e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
