#import <Foundation/Foundation.h>

#import <ScadeKit/EModelElement.h>


@class EModelElement;


/*PROTECTED REGION ID(bb40193b4a12f2c13fa28c25531e3636) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface ENamedElement : EModelElement


@property(nonatomic) NSString* _Nonnull name;


/*PROTECTED REGION ID(369d74fc8deee21c142393ba7bda42e8) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
