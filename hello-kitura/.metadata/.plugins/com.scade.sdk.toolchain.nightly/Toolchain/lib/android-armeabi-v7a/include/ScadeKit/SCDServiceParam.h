#import <Foundation/Foundation.h>

#import <ScadeKit/EParameter.h>


@class EParameter;


/*PROTECTED REGION ID(10cb1715d7b66ce0d12f5608491edbc0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceParam : EParameter


/*PROTECTED REGION ID(235291d6e5450b264170e245ce5efe72) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
