#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, SCDGraphicsHAlign) {
  SCDGraphicsHAlignLeft = 0,
  SCDGraphicsHAlignCenter = 1,
  SCDGraphicsHAlignRight = 2
};
typedef NS_ENUM(NSInteger, SCDGraphicsVAlign) {
  SCDGraphicsVAlignTop = 0,
  SCDGraphicsVAlignMiddle = 1,
  SCDGraphicsVAlignBottom = 2
};


#import <ScadeKit/SCDGraphicsPoint.h>

#import <ScadeKit/SCDGraphicsPointF.h>

#import <ScadeKit/SCDGraphicsDimension.h>

#import <ScadeKit/SCDGraphicsRectangle.h>

#import <ScadeKit/SCDGraphicsRGB.h>
