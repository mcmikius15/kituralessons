#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgConstraint.h>


@class SCDExprExpression;
@class SCDSvgConstraint;


/*PROTECTED REGION ID(14be89723dd8cf263c4f0360ed49ab4f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgDirectConstraint : SCDSvgConstraint


@property(nonatomic) SCDExprExpression* _Nullable expression;


/*PROTECTED REGION ID(1bba42e70c4942ed2f3a54e81228b516) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
