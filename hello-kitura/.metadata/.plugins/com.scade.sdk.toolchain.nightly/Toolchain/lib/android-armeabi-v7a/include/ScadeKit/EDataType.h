#import <Foundation/Foundation.h>

#import <ScadeKit/EClassifier.h>


@class EClassifier;


/*PROTECTED REGION ID(562e319cf67a283230c01c86cd990102) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface EDataType : EClassifier


@property(nonatomic, getter=isSerializable) BOOL serializable;


/*PROTECTED REGION ID(87a50884d60fa2b2b0090cedcfb56b11) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
