#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


typedef NS_ENUM(NSInteger, SCDSvgMeasurement);


/*PROTECTED REGION ID(ff9e4102cbba237bb4af9f3b78474dc7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgUnit : EObject


@property(nonatomic) float value;

@property(nonatomic) SCDSvgMeasurement measurement;


/*PROTECTED REGION ID(2302887a0da3a240329fed9609ec09ce) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
