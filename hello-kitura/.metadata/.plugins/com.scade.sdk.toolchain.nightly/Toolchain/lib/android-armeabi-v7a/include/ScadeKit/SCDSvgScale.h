#import <Foundation/Foundation.h>

#import <ScadeKit/SCDSvgTransform.h>


@protocol SCDSvgTransform;


/*PROTECTED REGION ID(231ca1987ecf94f01d2acb69888e725a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgScale : EObject <SCDSvgTransform>


@property(nonatomic) float scaleX;

@property(nonatomic) float scaleY;


/*PROTECTED REGION ID(f4a96b8e5d0caccc83af0d96bb47e493) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
