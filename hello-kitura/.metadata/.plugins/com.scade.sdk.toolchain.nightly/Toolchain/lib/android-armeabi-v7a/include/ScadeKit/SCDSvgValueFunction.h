#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(eda184d99cbd47c93cb0b7056fe3f5b1) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@protocol SCDSvgValueFunction <EObject>


- (id _Nullable)evaluate:(float)time;


/*PROTECTED REGION ID(a5dad3acc9f6ed4fefaae0b5ac94c771) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
