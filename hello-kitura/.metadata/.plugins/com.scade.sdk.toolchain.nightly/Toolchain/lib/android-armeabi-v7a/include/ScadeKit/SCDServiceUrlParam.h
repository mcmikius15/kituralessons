#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceParam.h>


@class SCDCommonRange;
@class SCDServiceParam;


/*PROTECTED REGION ID(ecfa16fba01ae4a25f3e7892f55fa6a4) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceUrlParam : SCDServiceParam


@property(nonatomic) SCDCommonRange* _Nonnull pos;


/*PROTECTED REGION ID(ea9389e071068d1cd5f94237471697d0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
