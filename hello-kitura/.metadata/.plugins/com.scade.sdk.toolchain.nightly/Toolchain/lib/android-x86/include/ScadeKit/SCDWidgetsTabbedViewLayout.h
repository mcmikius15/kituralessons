#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutGridLayout.h>


@class SCDLayoutGridLayout;


/*PROTECTED REGION ID(0f065e4a75a4f4b5a32a7dd2c494534d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsTabbedViewLayout : SCDLayoutGridLayout


/*PROTECTED REGION ID(eafd634cfd6b3b1c8c7903dcc7c11163) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
