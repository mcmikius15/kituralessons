#import <Foundation/Foundation.h>

#import <ScadeKit/SCDServiceAuth.h>


@protocol SCDServiceAuth;


/*PROTECTED REGION ID(b0f65a7d024f88b7f91eb560ddf16204) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDServiceNoAuth : EObject <SCDServiceAuth>


/*PROTECTED REGION ID(be13b68f10d4406b888fd35971bb7424) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
