#import <Foundation/Foundation.h>

#import <ScadeKit/SCDPlatformContactLabel.h>


@class SCDPlatformContactLabel;


/*PROTECTED REGION ID(4ce827fb617d734bce02350d09445991) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDPlatformContactUrl : SCDPlatformContactLabel


@property(nonatomic) NSString* _Nonnull url;


/*PROTECTED REGION ID(96503cdad739a382be5649806c2a80af) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
