#import <Foundation/Foundation.h>

#import <ScadeKit/SCDBindingBindingSelector.h>


@class SCDBindingBindingSelector;


/*PROTECTED REGION ID(dab21f46655799d3a9085f302e42c45e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDBindingCollectionSelector : SCDBindingBindingSelector


@property(nonatomic) long at;


/*PROTECTED REGION ID(9ae116dd49a96553322470b6f51f4f37) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
