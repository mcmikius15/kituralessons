#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsTextWidget.h>


@class SCDWidgetsTextWidget;


/*PROTECTED REGION ID(bef4802165a75bad6770323cdb22c65e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsSearchField : SCDWidgetsTextWidget


/*PROTECTED REGION ID(a9a727f9e2ae69c877d5753c6704c34a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
