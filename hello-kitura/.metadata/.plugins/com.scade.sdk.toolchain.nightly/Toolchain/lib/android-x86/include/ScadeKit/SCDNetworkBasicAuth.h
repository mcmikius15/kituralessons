#import <Foundation/Foundation.h>

#import <ScadeKit/SCDNetworkAuth.h>


@class SCDNetworkAuth;


/*PROTECTED REGION ID(51203ec9b466c328477848b3914093a4) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDNetworkBasicAuth : SCDNetworkAuth


@property(nonatomic) NSString* _Nonnull login;

@property(nonatomic) NSString* _Nonnull password;


/*PROTECTED REGION ID(ed8dfe579dca5e7601ce81bcf13bed3b) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
