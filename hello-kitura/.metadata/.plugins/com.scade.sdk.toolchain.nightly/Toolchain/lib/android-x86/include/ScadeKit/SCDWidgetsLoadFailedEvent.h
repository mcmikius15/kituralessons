#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsLoadEvent.h>


@class SCDWidgetsLoadEvent;


/*PROTECTED REGION ID(3bc1b9a32c9d183ab251b215c94b7e80) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsLoadFailedEvent : SCDWidgetsLoadEvent


@property(nonatomic) NSString* _Nonnull message;


/*PROTECTED REGION ID(b0f9e935a717d6db23abbfcd28d5785c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
