#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutGridStyle.h>
#import <ScadeKit/SCDWidgetsIStyle.h>


@protocol SCDWidgetsIStyle;

@class SCDLayoutGridStyle;


/*PROTECTED REGION ID(5b9a0a22f8e3f7d9cb8e1f37e13c1ea3) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsGridLayoutStyle : SCDLayoutGridStyle <SCDWidgetsIStyle>


/*PROTECTED REGION ID(040d7217d7a7561d52da97aae986e885) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
