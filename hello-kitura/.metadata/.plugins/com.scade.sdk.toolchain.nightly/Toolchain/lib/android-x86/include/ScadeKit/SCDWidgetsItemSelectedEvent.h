#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsItemEvent.h>


@class SCDWidgetsItemEvent;


/*PROTECTED REGION ID(780ed4e95a2fa7975b7b098863b96722) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsItemSelectedEvent : SCDWidgetsItemEvent


/*PROTECTED REGION ID(cf5ae2619c48521556e4021e4d7454c0) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
