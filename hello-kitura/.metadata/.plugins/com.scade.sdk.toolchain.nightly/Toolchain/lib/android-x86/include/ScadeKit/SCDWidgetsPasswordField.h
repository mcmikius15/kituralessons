#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsTextbox.h>


@class SCDWidgetsTextbox;


/*PROTECTED REGION ID(d2d2799369e1a8fdceefb04496751dd7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsPasswordField : SCDWidgetsTextbox


/*PROTECTED REGION ID(4314f93ce3fc4011b7e26d44d7f82f0e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
