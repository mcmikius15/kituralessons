#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprLeftRightExpression.h>


@class SCDExprLeftRightExpression;


/*PROTECTED REGION ID(7f9ecb036675871f9ea2d44cde50a67e) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprMultiplication : SCDExprLeftRightExpression


/*PROTECTED REGION ID(877a19dd9c36b20d3efbe79c61f730c7) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
