#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsEvent.h>


@class SCDWidgetsWidget;
@class SCDWidgetsEvent;


/*PROTECTED REGION ID(ea9b94f056811f9725a8c9699e826f75) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsItemEvent : SCDWidgetsEvent


@property(nonatomic) id _Nullable item;

@property(nonatomic) SCDWidgetsWidget* _Nullable element;


/*PROTECTED REGION ID(f7d32b3b44a7a731a7bbd6c07dbc750f) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
