#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>
#import <ScadeKit/SCDWidgetsClickable.h>


@protocol SCDWidgetsClickable;

@class SCDWidgetsWidget;


/*PROTECTED REGION ID(9e28a11b362a1b772d4c0d3b2e2c0244) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsCustomWidget : SCDWidgetsWidget <SCDWidgetsClickable>


/*PROTECTED REGION ID(fa18c2560a8d5aab079781a822d1e6ed) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
