#import <Foundation/Foundation.h>

#import <ScadeKit/SCDCoreResource.h>


@class SCDCoreResource;
@class EObject;


/*PROTECTED REGION ID(bc146ba9fe7c36e8aaaafce5acc91669) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDCoreXmiResource : SCDCoreResource


/*PROTECTED REGION ID(a8c0cdc3cbcf0cfe39e6c315119c2126) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
