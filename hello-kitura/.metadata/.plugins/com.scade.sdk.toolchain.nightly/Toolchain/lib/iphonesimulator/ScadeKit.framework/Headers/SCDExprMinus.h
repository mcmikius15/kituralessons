#import <Foundation/Foundation.h>

#import <ScadeKit/SCDExprLeftRightExpression.h>


@class SCDExprLeftRightExpression;


/*PROTECTED REGION ID(2513657948b673c8136017d8bd42b123) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDExprMinus : SCDExprLeftRightExpression


/*PROTECTED REGION ID(93e8ff179259e9a822ede116d8d2899c) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
