#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class SCDBindingBinding;


/*PROTECTED REGION ID(65dd2c525095b84b0abe04591967ebc5) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDBindingBindingModel : EObject


@property(nonatomic) NSArray<SCDBindingBinding*>* _Nonnull bindings;


/*PROTECTED REGION ID(cbb0f91f6296f9e6b528fef13b00642a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
