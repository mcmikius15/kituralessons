#import <Foundation/Foundation.h>

#import <ScadeKit/SCDWidgetsWidget.h>


@class SCDWidgetsListElement;
@class SCDWidgetsList;
@class SCDWidgetsWidget;


/*PROTECTED REGION ID(7bf4072dafd8cfa356dee5cedaa52bf9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDWidgetsListTemplate : SCDWidgetsWidget


@property(nonatomic) SCDWidgetsListElement* _Nullable element;

@property(nonatomic, readonly) SCDWidgetsList* _Nullable list;


/*PROTECTED REGION ID(3e67fac61597db0640ce15940d4d954d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
