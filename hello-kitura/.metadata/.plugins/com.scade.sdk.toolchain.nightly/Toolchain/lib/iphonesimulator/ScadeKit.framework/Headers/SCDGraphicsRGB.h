#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


/*PROTECTED REGION ID(ca6509da353b21d58335481d8a403c5a) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDGraphicsRGB : EObject


@property(nonatomic) long red;

@property(nonatomic) long green;

@property(nonatomic) long blue;

@property(nonatomic) long alpha;


/*PROTECTED REGION ID(c91c6cb24d227901d0e5795c85ac07bb) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
