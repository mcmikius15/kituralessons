#import <Foundation/Foundation.h>

#import <ScadeKit/EObject.h>


@class EStructuralFeature;


/*PROTECTED REGION ID(0e08365320bcf84534da6489fb38d26d) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDSvgConstraint : EObject


@property(nonatomic) NSString* _Nonnull exprStr;

@property(nonatomic) EStructuralFeature* _Nullable feature;


/*PROTECTED REGION ID(52dfb7d5042946ba49b9639f8a28f865) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
