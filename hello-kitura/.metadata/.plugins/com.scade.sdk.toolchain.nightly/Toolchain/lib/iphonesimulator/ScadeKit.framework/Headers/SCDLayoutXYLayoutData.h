#import <Foundation/Foundation.h>

#import <ScadeKit/SCDLayoutLayoutData.h>


@class SCDGraphicsRectangle;
@class SCDLayoutLayoutData;


/*PROTECTED REGION ID(96e3c5be7f3e0bd08ad05e02141e2ea9) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/


SCADE_API
@interface SCDLayoutXYLayoutData : SCDLayoutLayoutData


@property(nonatomic) SCDGraphicsRectangle* _Nonnull constraint;


/*PROTECTED REGION ID(b359abe9013a085ce718d07a9db83352) START*/
// Please, enable the protected region if you add manually written code.
// To do this, add the keyword ENABLED before START.
/*PROTECTED REGION END*/

@end
